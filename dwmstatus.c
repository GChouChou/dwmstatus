/*
 * Copy me if you can.
 * by 20h
 */

#define _BSD_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <X11/Xlib.h>

//char *tzargentina = "America/Buenos_Aires";
//char *tzutc = "UTC";
char *tzmtl = "America/Montreal";

static Display *dpy;

char *
smprintf(char *fmt, ...)
{
	va_list fmtargs;
	char *ret;
	int len;

	va_start(fmtargs, fmt);
	len = vsnprintf(NULL, 0, fmt, fmtargs);
	va_end(fmtargs);

	ret = malloc(++len);
	if (ret == NULL) {
		perror("malloc");
		exit(1);
	}

	va_start(fmtargs, fmt);
	vsnprintf(ret, len, fmt, fmtargs);
	va_end(fmtargs);

	return ret;
}

void
settz(char *tzname)
{
	setenv("TZ", tzname, 1);
}

int
parse_netdev(unsigned long long int *receivedabs, unsigned long long int *sentabs)
{
	char buf[255];
	char *datastart;
	static int bufsize;
	int rval;
	FILE *devfd;
	unsigned long long int receivedacc, sentacc;

	bufsize = 255;
	devfd = fopen("/proc/net/dev", "r");
	rval = 1;

	// Ignore the first two lines of the file
	fgets(buf, bufsize, devfd);
	fgets(buf, bufsize, devfd);

	while (fgets(buf, bufsize, devfd)) {
	    if ((datastart = strstr(buf, "lo:")) == NULL) {
		datastart = strstr(buf, ":");

		// With thanks to the conky project at http://conky.sourceforge.net/
		sscanf(datastart + 1, "%llu  %*d     %*d  %*d  %*d  %*d   %*d        %*d       %llu",\
		       &receivedacc, &sentacc);
		*receivedabs += receivedacc;
		*sentabs += sentacc;
		rval = 0;
	    }
	}

	fclose(devfd);
	return rval;
}

void
calculate_speed(char *speedstr, unsigned long long int newval, unsigned long long int oldval)
{
	double speed;
	speed = (newval - oldval) / 1024.0;
	if (speed > 1024.0) {
	    speed /= 1024.0;
	    sprintf(speedstr, "%.3f MB/s", speed);
	} else {
	    sprintf(speedstr, "%.2f KB/s", speed);
	}
}

char *
get_netusage(unsigned long long int *rec, unsigned long long int *sent)
{
	unsigned long long int newrec, newsent;
	newrec = newsent = 0;
	char downspeedstr[15], upspeedstr[15];
	static char retstr[42];
	int retval;

	retval = parse_netdev(&newrec, &newsent);
	if (retval) {
	    fprintf(stdout, "Error when parsing /proc/net/dev file.\n");
	    exit(1);
	}

	calculate_speed(downspeedstr, newrec, *rec);
	calculate_speed(upspeedstr, newsent, *sent);

	sprintf(retstr, " \uf545:%s \uf55d:%s", downspeedstr, upspeedstr);

	*rec = newrec;
	*sent = newsent;
	return retstr;
}

char *
mktimes(char *fmt, char *tzname)
{
	char buf[129];
	time_t tim;
	struct tm *timtm;

	settz(tzname);
	tim = time(NULL);
	timtm = localtime(&tim);
	if (timtm == NULL)
		return smprintf("");

	if (!strftime(buf, sizeof(buf)-1, fmt, timtm)) {
		fprintf(stderr, "strftime == 0\n");
		return smprintf("");
	}

	return smprintf("%s", buf);
}

void
setstatus(char *str)
{
	XStoreName(dpy, DefaultRootWindow(dpy), str);
	XSync(dpy, False);
}

char *
loadavg(void)
{
	double avgs[3];

	if (getloadavg(avgs, 3) < 0)
		return smprintf("");

	return smprintf("%.2f %.2f %.2f", avgs[0], avgs[1], avgs[2]);
}

char *
readfile(char *base, char *file)
{
	char *path, line[513];
	FILE *fd;

	memset(line, 0, sizeof(line));

	path = smprintf("%s/%s", base, file);
	fd = fopen(path, "r");
	free(path);
	if (fd == NULL)
		return NULL;

	if (fgets(line, sizeof(line)-1, fd) == NULL)
		return NULL;
	fclose(fd);

	return smprintf("%s", line);
}

char *
getbattery(char *base)
{
	char *co;
    char *status;
	int cap;
    float charge,current;
    int hours;

	cap = -1;

	co = readfile(base, "present");
	if (co == NULL)
		return smprintf("");
	if (co[0] != '1') {
		free(co);
		return smprintf("not present");
	}
	free(co);

	co = readfile(base, "capacity");
	if (co == NULL) {
        return smprintf("");
	}
	sscanf(co, "%d", &cap);
	free(co);

	co = readfile(base, "status");
	if (!strncmp(co, "Discharging", 11)) {
        if (cap < 20) {
            status = "\uf58a";
        } else if (cap < 60) {
            status = "\uf57d";
        } else {
            status = "\uf57f";
        }
        free(co);
        co = readfile(base, "charge_now");
        if (co == NULL)
        {
            return smprintf("");
        }
        sscanf(co, "%f", &charge);
        free(co);
	} else if(!strncmp(co, "Charging", 8)) {
        if (cap < 20) {
            status = "\uf585";
        } else if (cap < 60) {
            status = "\uf588";
        } else {
            status = "\uf584";
        }
        free(co);
        co = readfile(base, "charge_now");
        if (co == NULL)
        {
            return smprintf("");
        }
        sscanf(co, "%f", &charge);
        free(co);
        co = readfile(base, "charge_full");
        if (co == NULL)
        {
            return smprintf("");
        }
        sscanf(co, "%f", &current);
        free(co);
        charge = current - charge;
	} else {
		status = "?";
	}

	co = readfile(base, "current_now");
    if (co == NULL)
    {
        return smprintf("");
    }
	sscanf(co, "%f", &current);
	free(co);


	if (cap < 0)
		return smprintf("invalid");

    // get time left in float
    charge = charge / current;
    hours = (int) charge;
    return smprintf("%s:%.0f%% %d:%02.0f", status, (float) cap,
            hours, (charge - hours) * 60.0f);
}

int
main(void)
{
	char *status = 0;
	//char *avgs = 0;
	char *bat = 0;
	char *tmmtl = 0;
    static unsigned long long int rec, sent;
	char *netstats = 0;
    int counter = 0;


	if (!(dpy = XOpenDisplay(NULL))) {
		fprintf(stderr, "dwmstatus: cannot open display.\n");
		return 1;
	}

    parse_netdev(&rec, &sent);
	for (;;sleep(1), counter++) {
		//free(avgs);
		//avgs = loadavg();
        if (counter%60 == 0) {
            free(tmmtl);
            tmmtl = mktimes("%a %d %b %H:%M %Z %Y", tzmtl);
            counter = 0;
        }
        free(bat);
        bat = getbattery("/sys/class/power_supply/BAT0");
        netstats = get_netusage(&rec, &sent);

		free(status);
		status = smprintf("  N:%s | %s | \uf64f %s",
                netstats, bat, tmmtl);
		setstatus(status);

        //free(netstats);
		//free(bat1);
		//free(tmar);
		//free(tmutc);
		//free(tmbln);
	}

	XCloseDisplay(dpy);

	return 0;
}

